﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeamProject
{
    public class Reservation
    {
        [Key]
        public int ReservationId { get; set; }

        public int MemberId { get; set; }
        public Member Member { get; set; }

        public int LibrarianId { get; set; }
        public Librarian Librarian { get; set; }

        [Required]
        public string Title { get; set; }
        public Booklist BookTitle { get; set; }

        [Range(10, 13, ErrorMessage = "ISBN must be 10 to 13"), Required]
        public int ISBN { get; set; }
        public Booklist BookISBN { get; set; }

        public DateTime CheckOut { get; set; }
        public DateTime CheckIn { get; set; }

        public override string ToString()
        {
            return string.Format("MemberID {0}, Librarian ID {1}, Title {2}, ISBN {3}, CheckOut {4}, CheckIn {5}", MemberId, LibrarianId, Title, ISBN, CheckOut, CheckIn);
        }


    }
}
