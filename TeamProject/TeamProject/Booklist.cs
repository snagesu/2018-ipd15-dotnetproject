﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeamProject
{
    public class Booklist
    {
        [Key]
        public int BookId { get; set; }

        [Required]
        public int ISBN { get; set; }

        public byte[] Photo { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public  string Publisher { get; set; }

        [Required]
        public int Year { get; set; }
        
        public virtual ICollection<Reservation> Reservation { get; set; }

        public override string ToString()
        {
            return string.Format("ISBN{0}, Photo{1}, Title{2}, Publisher{3}, Year{4}", ISBN, Photo, Title, Publisher, Year);
        }


    }
}
