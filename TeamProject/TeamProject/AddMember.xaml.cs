﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for AddMember.xaml
    /// </summary>
    public partial class AddMember : Window
    {
        private Member currMember;
        string imagePath;

        public AddMember(Window parent, Member __currMember = null)
        {
            InitializeComponent();
            this.Owner = parent;
            Globals.ctx = new BookContext();
            currMember = __currMember;

            Globals.ctx = new BookContext();

            if (currMember != null)
            {
                MemoryStream memoryStream = new MemoryStream(currMember.Photo);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.EndInit();
                MemberImage.Source = bitmapImage;

                lbMemberId.Content = currMember.MemberId;
                tbFirstname.Text = currMember.Firstname;
                tbLastname.Text = currMember.Lastname;
            }
        }

        private void Button_AddClick(object sender, RoutedEventArgs e)
        {
            string name = @"^[a-zA-Z]{2,20}$";
            Match result = Regex.Match(tbFirstname.Text, name);


            if (!result.Success)
            {
                MessageBox.Show("Name should be 2 to 20 characters");
                return;
            }
            else
                name = tbFirstname.Text;

            string lastName = @"^[a-zA-Z]{2,20}$";
            Match result1 = Regex.Match(tbLastname.Text, lastName);


            if (!result1.Success)
            {
                MessageBox.Show("Last Name should be 2 to 20 characters");
                return;
            }
            else
                lastName = tbLastname.Text;

            if (currMember == null)
            {
                Member m = new Member()
                {
                    Firstname = name,
                    Lastname = lastName,
                    Photo = File.ReadAllBytes(imagePath),
                };
                Globals.ctx.Group.Add(m);
            }
            else
            {
                currMember.Firstname = name;
                currMember.Lastname = lastName;
                MemoryStream memoryStream = new MemoryStream();
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(MemberImage.Source as BitmapImage));
                encoder.Save(memoryStream);
                currMember.Photo = memoryStream.ToArray();
            }
            Globals.ctx.SaveChanges();
            MessageBox.Show("Member Added");
            this.DialogResult = true;
        }

        private void Button_CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Button_UploadClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            if (dialog.ShowDialog() == true)
            {
                imagePath = dialog.FileName;
                MemberImage.Source = new BitmapImage(new Uri(imagePath));
            }
        }
    }
}


