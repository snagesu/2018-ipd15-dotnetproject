﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.ComponentModel;

namespace TeamProject
{

    public class UserDisplayName {
        public static string displayName;
    }


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public delegate void StatusUpdaterDelegateType(String s);
        public StatusUpdaterDelegateType UpdateStatus;

        public MainWindow()
        {




            Globals.ctx = new BookContext();
            
            InitializeComponent();
            CheckIn.SelectedDate = DateTime.Now;
            Checkout.SelectedDate = DateTime.Now;
            CheckIn.BlackoutDates.AddDatesInPast();
            Checkout.BlackoutDates.AddDatesInPast();
            refreshList();
            refreshList1();
            refreshList2();
        }


       public void refreshList()
        {
            try
            {
                Globals.ctx = new BookContext();
                var g = (from m in Globals.ctx.Group select m).ToList();
                Globals.memberList = new ObservableCollection<Member>(g);
                lvMember.DataContext = Globals.memberList;

            }
            catch (System.IO.InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public void refreshList1()
        {
            try
            {
                Globals.ctx = new BookContext();
                var x = (from l in Globals.ctx.Curator select l).ToList();
                Globals.librarianList = new ObservableCollection<Librarian>(x);
                lvLibrarian.DataContext = Globals.librarianList;
            }
            catch (System.IO.InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void refreshList2()
        {
            try
            {
                Globals.ctx = new BookContext();
                var z = (from b in Globals.ctx.Booklet select b).ToList();
                Globals.bookList = new ObservableCollection<Booklist>(z);
                lvBook.ItemsSource = Globals.bookList;

            }
            catch (System.IO.InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void Button_MemberClick(object sender, RoutedEventArgs e)
        {
            AddMember dlg = new AddMember(this);
            if (dlg.ShowDialog() == true)
            {
                refreshList();
                MessageBox.Show("Success!");
            }
        }

        private void Button_librarianClick(object sender, RoutedEventArgs e)
        {
            AddLibrarian dlg = new AddLibrarian(this);
            if (dlg.ShowDialog() == true)
            {
                refreshList1();
                MessageBox.Show("Success!");

            }
        }

        private void Button_BookClick(object sender, RoutedEventArgs e)
        {
            AddBook dlg = new AddBook(this);
            if (dlg.ShowDialog() == true)
            {
                refreshList2();
                MessageBox.Show("Success!");
            }
        }

        private void MenuItem_MemberDeleteClick(object sender, RoutedEventArgs e)
        {
            if (lvMember.SelectedIndex == -1) return;
            Member m = lvMember.SelectedItem as Member;
            var result = MessageBox.Show("Are you sure you want to delete this item?\n" + m, "Delete Confirmation",
            MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                Globals.memberList.Remove(m);
                lvMember.Items.Refresh();
            }

        }

        private void MenuItem_LibrarianDeleteClick(object sender, RoutedEventArgs e)
        {
            if (lvLibrarian.SelectedIndex == -1) return;
            Librarian l = lvLibrarian.SelectedItem as Librarian;
            var result1 = MessageBox.Show("Are you sure you want to delete this item?\n" + l, "Delete Confirmation",
            MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result1 == MessageBoxResult.OK)
            {
                Globals.librarianList.Remove(l);
                lvLibrarian.Items.Refresh();
            }
        }

        private void MenuItem_BookDeleteClick(object sender, RoutedEventArgs e)
        {
            if (lvBook.SelectedIndex == -1) return;
            Booklist b = lvBook.SelectedItem as Booklist;
            var result2 = MessageBox.Show("Are you sure you want to delete this item?\n" + b, "Delete Confirmation",
            MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result2 == MessageBoxResult.OK)
            {
                Globals.bookList.Remove(b);
                lvBook.Items.Refresh();
            }
        }

        private void Button_ReservationClick(object sender, RoutedEventArgs e)
        {
            AddReservation dlg = new AddReservation(this);
            if (dlg.ShowDialog() == true)
            {
                refreshList2();
                MessageBox.Show("Success!");
            }

        }

        private bool librarianFilter(object obj)
        {
            Librarian librarian = obj as Librarian;
            if (librarian == null) return false;

            string textFilter = tbLibrarian.Text;
            if (textFilter.Trim().Length == 0) return true;
            if (librarian.Firstname.ToLower().Contains(textFilter.ToLower()) || librarian.Lastname.ToLower().Contains(textFilter.ToLower())) return true;
            return false;
        }

        private bool MemberFilter(object obj)
        {
            Member member = obj as Member;
            if (member == null) return false;

            string textFilter = tbMembers.Text;
            if (textFilter.Trim().Length == 0) return true;
            if (member.Firstname.ToLower().Contains(textFilter.ToLower()) || member.Lastname.ToLower().Contains(textFilter.ToLower())) return true;
            return false;
        }

        private bool BookFilter(object obj)
        {
            Booklist booklist = obj as Booklist;
            if (booklist == null) return false;

            string textFilter = tbBooks.Text;
            if (textFilter.Trim().Length == 0) return true;
            if (booklist.Publisher.ToLower().Contains(textFilter.ToLower()) || booklist.Title.ToLower().Contains(textFilter.ToLower()) || booklist.ISBN.ToString().Contains(textFilter) || booklist.Year.ToString().Contains(textFilter)) return true;
            return false;
        }

        private void tbLibrarian_TextChanged(object sender, TextChangedEventArgs e)
        {
            ICollectionView view = CollectionViewSource.GetDefaultView(lvLibrarian.ItemsSource);
            view.Filter = null;
            view.Filter = new Predicate<object>(librarianFilter);
        }

        private void tbMembers_TextChanged(object sender, TextChangedEventArgs e)
        {
            ICollectionView view = CollectionViewSource.GetDefaultView(lvMember.ItemsSource);
            view.Filter = null;
            view.Filter = new Predicate<object>(MemberFilter);
        }

        private void tbBooks_TextChanged(object sender, TextChangedEventArgs e)
        {
            ICollectionView view = CollectionViewSource.GetDefaultView(lvBook.ItemsSource);
            view.Filter = null;
            view.Filter = new Predicate<object>(BookFilter);
        }

        private void Librarian_ExportClick(object sender, RoutedEventArgs e)
        {

            string appRootDir = new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.FullName;
            try
            {

                using (FileStream fs = new FileStream(appRootDir + "/PDFs/" + "F.pdf", FileMode.Create, FileAccess.Write, FileShare.None))

                using (Document doc = new Document())

                using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                {
                    writer.SetEncryption(PdfWriter.STRENGTH40BITS, null, null, PdfWriter.ALLOW_COPY);
                    doc.Open();
                    doc.Add(new iTextSharp.text.Paragraph("Librarian Exporting :"));
                    var lists = lvLibrarian.SelectedItems;
                    int count = 1;
                    foreach (var x in lists)
                    {
                        string librarian = count + " :  " + x + "\n";
                        Phrase phrase2 = new Phrase(librarian);
                        doc.Add(phrase2);
                        count++;
                    }
                    doc.Close();


                    {

                    }
                }
            }

            catch (DocumentException de)
            {
                throw de;
            }

            catch (IOException ioe)
            {
                throw ioe;
            }
        }


        private void Member_ExportClick(object sender, RoutedEventArgs e)
        {

            string appRootDir = new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.FullName;
            try
            {

                using (FileStream fs = new FileStream(appRootDir + "/PDFs/" + "Member.pdf", FileMode.Create, FileAccess.Write, FileShare.None))

                using (Document doc = new Document())

                using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                {
                    writer.SetEncryption(PdfWriter.STRENGTH40BITS, null, null, PdfWriter.ALLOW_COPY);
                    doc.Open();
                    doc.Add(new iTextSharp.text.Paragraph("Member : \n"));
                    int count = 1;
                    var lists2 = lvMember.SelectedItems;
                    foreach (var item in lists2)
                    {
                        string member = count + "  :  " + item + "\n";
                        Phrase phrase2 = new Phrase(member);
                        doc.Add(phrase2);
                        count++;
                    }

                    doc.Close();
                }
            }

            catch (DocumentException de)
            {
                throw de;
            }

            catch (IOException ioe)
            {
                throw ioe;
            }
        }

        private void Booklist_ExportClick(object sender, RoutedEventArgs e)
        {
            string appRootDir = new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.FullName;
            try
            {

                using (FileStream fs = new FileStream(appRootDir + "/PDFs/" + "Booklist.pdf", FileMode.Create, FileAccess.Write, FileShare.None))

                using (Document doc = new Document())

                using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                {
                    writer.SetEncryption(PdfWriter.STRENGTH40BITS, null, null, PdfWriter.ALLOW_COPY);
                    doc.Open();
                    doc.Add(new iTextSharp.text.Paragraph("Booklist : \n"));
                    var lists3 = lvBook.SelectedItems;
                    int count = 1;
                    foreach (var item in lists3)
                    {
                        string booklist = count + " list :  " + item + "\n";
                        Phrase phrase2 = new Phrase(booklist);
                        doc.Add(phrase2);
                        count++;
                    }

                    doc.Close();
                }
            }

            catch (DocumentException de)
            {
                throw de;
            }

            catch (IOException ioe)
            {
                throw ioe;
            }
        }

        private void lvBook_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            Booklist currBook = lvBook.SelectedItem as Booklist;
            if (currBook == null) return;

            AddBook dlg = new AddBook(this, currBook);
            if (dlg.ShowDialog() == true) {

                refreshList2();
                if (UpdateStatus != null) {
                    UpdateStatus("Book updated!");

                }
                MessageBox.Show("Success!");
            }
        }

        private void lvMember_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Member currMember = lvMember.SelectedItem as Member;
            if (currMember == null) return;

            AddMember dlg = new AddMember(this, currMember);
            if (dlg.ShowDialog() == true)
            {

                refreshList();
                if (UpdateStatus != null)
                {
                    UpdateStatus("Member updated!");

                }
                MessageBox.Show("Success!");
            }
        }

        private void lvLibrarian_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Librarian currLibrarian = lvLibrarian.SelectedItem as Librarian;
            if (currLibrarian == null) return;

            AddLibrarian dlg = new AddLibrarian(this, currLibrarian);
            if (dlg.ShowDialog() == true)
            {

                refreshList1();
                if (UpdateStatus != null)
                {
                    UpdateStatus("Librarian updated!");

                }
                MessageBox.Show("Success!");
            }
        }

        private void Button_InsertClick(object sender, RoutedEventArgs e)
        {




        }


    }
}



