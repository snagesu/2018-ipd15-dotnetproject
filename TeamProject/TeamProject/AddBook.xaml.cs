﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for AddBook.xaml
    /// </summary>
    public partial class AddBook : Window
    {
        private Booklist currBook;
        string imagePath;

        public AddBook(Window parent, Booklist __currBook = null)
        {
            InitializeComponent();
            this.Owner = parent;
            Globals.ctx = new BookContext();
            currBook = __currBook;



            if (currBook != null)
            {
                MemoryStream memoryStream = new MemoryStream(currBook.Photo);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.EndInit();
                BookImage.Source = bitmapImage;

                lbBookId.Content = currBook.BookId;
                tbISBN.Text = currBook.ISBN + "";
                tbTitle.Text = currBook.Title;
                tbPublisher.Text = currBook.Publisher;
                tbYear.Text = currBook.Year + "";
            }
        }



        private void Button_AddClick(object sender, RoutedEventArgs e)
        {

            int isbn = 0;
            if (!(int.TryParse(tbISBN.Text.Trim(' '), out isbn)) || isbn == 10 || isbn == 13)
            {
                MessageBox.Show("ISBN must have 10-13 digits");
                return;
            }
            else
            {
                isbn = int.Parse(tbISBN.Text.Trim(' '));
            }

            string title;
            if ((tbTitle.Text.Trim(' ').Length < 2) || (tbTitle.Text.Trim(' ').Length > 30))
            {
                MessageBox.Show("title should be 2 to 20 characters");
                return;
            }
            else
            {
                title = tbTitle.Text;
            }

            string publisher;
            if ((tbPublisher.Text.Trim(' ').Length < 2) || (tbPublisher.Text.Trim(' ').Length > 30))
            {
                MessageBox.Show("Publisher should be 2 to 20 characters");
                return;
            }
            else
            {
                publisher = tbPublisher.Text;
            }

            int year = 0;
            if (!(int.TryParse(tbYear.Text.Trim(' '), out year)) || (year < 2000) || (year > 2020))
            {
                MessageBox.Show("Year must be a number between 2000 and 2020");
                return;
            }
            else
            {
                year = int.Parse(tbYear.Text.Trim(' '));
            }


            if (currBook == null)
            {
                Booklist b = new Booklist()
                {
                    ISBN = isbn,
                    Photo = File.ReadAllBytes(imagePath),
                    Title = title,
                    Publisher = publisher,
                    Year = year,
                };
                Globals.ctx.Booklet.Add(b);
            }
            else
            {
                currBook.ISBN = isbn;

                MemoryStream memoryStream = new MemoryStream();
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(BookImage.Source as BitmapImage));
                encoder.Save(memoryStream);
                currBook.Photo = memoryStream.ToArray();

                currBook.Title = title;
                currBook.Publisher = publisher;
                currBook.Year = year;
            }
            Globals.ctx.SaveChanges();
            MessageBox.Show("Book Added");
            this.DialogResult = true;
        }


        private void Button_CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Button_UploadClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            if (dialog.ShowDialog() == true)
            {
                imagePath = dialog.FileName;
                BookImage.Source = new BitmapImage(new Uri(imagePath));


            }
        }
    }
}
