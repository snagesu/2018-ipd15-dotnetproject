﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeamProject
{
    public class User
    {

        [Key]
        public int UserId { get; set; }

        [MaxLength(20), Required]
        public string Username { get; set; }

        [Required]
        //RegularExpression("^.{4,12}$", ErrorMessage ="Password must be 4 to 8 characters"), 
        public string Password { get; set; }

        public virtual ICollection<Librarian> Librarian { get; set; }



    }
}
