﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;



namespace TeamProject
{
    /// <summary>
    /// Interaction logic for AddLibrarian.xaml
    /// </summary>
    public partial class AddLibrarian : Window
    {
        private Librarian currLibrarian;
        string imagePath;

        public AddLibrarian(Window parent, Librarian __currLibrarian = null)
        {
            InitializeComponent();
            this.Owner = parent;
            Globals.ctx = new BookContext();
            currLibrarian = __currLibrarian;


            if (currLibrarian != null)
            {
                MemoryStream memoryStream = new MemoryStream(currLibrarian.Photo);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.EndInit();
                LibrarianImage.Source = bitmapImage;

                lbldpUsername.Content = currLibrarian.Username;
                lbLibrarianId.Content = currLibrarian.LibrarianId;
                tbFirstname.Text = currLibrarian.Firstname;
                tbLastname.Text = currLibrarian.Lastname;

            }
            
        }

        private void Button_AddClick(object sender, RoutedEventArgs e)
        {
            string name = @"^[a-zA-Z]{2,20}$";
            Match result = Regex.Match(tbFirstname.Text, name);


            if (!result.Success)
            {
                MessageBox.Show("Name should be 2 to 20 characters");
                return;
            }
            else
            {
                name = tbFirstname.Text;
            }

            string lastName = @"^[a-zA-Z]{2,20}$";
            Match result1 = Regex.Match(tbLastname.Text, lastName);


            if (!result1.Success)
            {
                MessageBox.Show("Last Name should be 2 to 20 characters");
                return;
            }
            else
            {
                lastName = tbLastname.Text;
            }

            if (currLibrarian == null)
            {
                Librarian l = new Librarian()
                {
                    Firstname = name,
                    Lastname = lastName,
                    Photo = File.ReadAllBytes(imagePath),
                };
                Globals.ctx.Curator.Add(l);
            }
            else
            {
                currLibrarian.Firstname = name;
                currLibrarian.Lastname = lastName;
                MemoryStream memoryStream = new MemoryStream();
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(LibrarianImage.Source as BitmapImage));
                encoder.Save(memoryStream);
                currLibrarian.Photo = memoryStream.ToArray();
            }
            Globals.ctx.SaveChanges();
            MessageBox.Show("Librarian Added");
            this.DialogResult = true;
        }

        private void Button_CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Button_UploadClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            if (dialog.ShowDialog() == true)
            {
                imagePath = dialog.FileName;
                LibrarianImage.Source = new BitmapImage(new Uri(imagePath));
            }

        }
    }
}
