﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeamProject
{
    public class Book
    {
        [Key]
        [Required] //Required indentity 1,1
        public int BookId { get; set; }

        [Range(10, 13, ErrorMessage = "ISBN must be 10 to 13"), Required]
        public int ISBN { get; set; }

        //How to limit size by KB
        public byte[] Photo { get; set; }

        [Required]
        public string Title { get; set; }

        public string Publisher { get; set; }

        [RegularExpression("^[12][0-9]{3}$", ErrorMessage = "Must be a valid year")]
        public int Year { get; set; }

        public CoverEnum Cover { get; set; }
        public enum CoverEnum { Papercover = 0, Hardcover = 1, Ebook = 2 };

        //Copy Missing
        public ICollection<Rental> Rental { get; set; }
    }
}
