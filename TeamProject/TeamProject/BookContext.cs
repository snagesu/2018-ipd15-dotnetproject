﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
    public class BookContext : DbContext
    {

        public BookContext() : base("name = DbConnectionBook") { }

        public DbSet<Member> Group { get; set; }

        public DbSet<Librarian> Curator { get; set; }

        public DbSet<Booklist> Booklet { get; set; }

        public DbSet<User> EndUser { get; set; }

        public DbSet<Reservation> Reserve { get; set; }
    }
}
