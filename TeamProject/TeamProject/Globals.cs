﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
    class Globals
    {

        public static BookContext ctx;
        public static ObservableCollection<Member> memberList;
        public static ObservableCollection<Librarian> librarianList;
        public static ObservableCollection<Booklist> bookList;
    }
}
