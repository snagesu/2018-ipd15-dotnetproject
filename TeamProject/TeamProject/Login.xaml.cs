﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {


        public Login()
        {
            InitializeComponent();

        }

        private void Button_LoginClick(object sender, RoutedEventArgs e)
        {

            using (Globals.ctx = new BookContext())
            {
                var user = Globals.ctx.EndUser.FirstOrDefault(u => u.Username == tbUsername.Text);
                if (user != null)
                {

                    if (user.Password == pbPassword.Password)
                    {
                        this.Hide();
                        MainWindow w1 = new MainWindow();
                        w1.ShowDialog();
                        this.Show();    
                    }
                    else
                        MessageBox.Show("Password is incorrect!");
                }
                else
                {
                    MessageBox.Show("Cannot be empty values!");
                }

            }


        }


    }
}



