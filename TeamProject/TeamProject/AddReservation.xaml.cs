﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for AddReservation.xaml
    /// </summary>
    public partial class AddReservation : Window
    {

        public Reservation currReservation;

        public AddReservation(Window parent, Reservation __currReservation = null)
        {
            InitializeComponent();
            this.Owner = parent;

            Globals.ctx = new BookContext();
            currReservation = __currReservation;
        }

        private void tbMembers_TextChanged(object sender, TextChangedEventArgs e)
        {


        }

        private void tbLibrarians_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void tbBooklist_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        

        private void Button_CancelBookClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Button_CancelLibrarianClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Button_CancelMemberClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
