﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeamProject
{
    public class Librarian
    {
        

        [Key] //AI indentity 1,1
        public int LibrarianId { get; set; }

        public string Username { get; set; }
        public User User { get; set; }

        [MaxLength(20), Required]
        public string Firstname { get; set; }

        [MaxLength(20), Required]
        public string Lastname { get; set; }

        //For an image
        [Required]
        public byte[] Photo { get; set; }

        public virtual ICollection<Reservation> Reservation { get; set; }
        

        public override string ToString()
        {
            return string.Format("{0}, {1} , {2}", Firstname, Lastname, Photo);
        }

    }
}
